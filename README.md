# hems-exporter

```
$ docker build -t hems-exporter .

# use --net=host for UDP multicast
$ docker run -d --restart=always --net=host hems-exporter
```
